#-------------------------------------------------
#相机
#-------------------------------------------------

INCLUDEPATH         += $$PWD/src

HEADERS += \
    $$PWD/src/qtcamera.h \
    $$PWD/src/myvideosurface.h \
    $$PWD/src/rknn_ssd.h \
    $$PWD/src/rknn_ssd_process.h \
    $$PWD/src/imageutil.h


SOURCES += \
    $$PWD/src/qtcamera.cpp \
    $$PWD/src/myvideosurface.cpp \
    $$PWD/src/rknn_ssd.cpp \
    $$PWD/src/rknn_ssd_process.cpp
