// Copyright (c) 2021 by Rockchip Electronics Co., Ltd. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/*-------------------------------------------
                Includes
-------------------------------------------*/
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <fstream>
#include <iostream>
#include <sys/time.h>
#include "rknn_ssd_process.h"

using namespace std;
using namespace cv;

/*-------------------------------------------
                  Functions
-------------------------------------------*/

static void printRKNNTensor(rknn_tensor_attr *attr)
{
    printf("index=%d name=%s n_dims=%d dims=[%d %d %d %d] n_elems=%d size=%d fmt=%d type=%d qnt_type=%d fl=%d zp=%d scale=%f\n",
           attr->index, attr->name, attr->n_dims, attr->dims[0], attr->dims[1], attr->dims[2], attr->dims[3],
           attr->n_elems, attr->size, 0, attr->type, attr->qnt_type, attr->fl, attr->zp, attr->scale);
}

unsigned char *RknnSsdModel::LoadModel(const char *filename, int *model_size)
{
    FILE *fp = fopen(filename, "rb");
    if (fp == nullptr)
    {
        printf("fopen %s fail!\n", filename);
        return NULL;
    }

    fseek(fp, 0, SEEK_END);

    int model_len = ftell(fp);
    unsigned char *model = (unsigned char *)malloc(model_len);

    fseek(fp, 0, SEEK_SET);

    if (model_len != fread(model, 1, model_len, fp))
    {
        printf("fread %s fail!\n", filename);
        free(model);
        return NULL;
    }

    *model_size = model_len;

    if (fp)
    {
        fclose(fp);
    }
    return model;
}

/*-------------------------------------------
                  Main Function
-------------------------------------------*/
int RknnSsdModel::RknnInit(const char *model_path)
{
    int ret = 0;
    int model_len = 0;

    // Load RKNN Model
    printf("Loading model ...\n");
    m_pModel = LoadModel(model_path, &model_len);

    printf("rknn_init ...\n");
    ret = rknn_init(&m_rknnCtx, m_pModel, model_len, 0, NULL);
    if (ret < 0)
    {
        printf("rknn_init fail! ret=%d\n", ret);
        return -1;
    }

    // Get Model Input Output Info
    ret = rknn_query(m_rknnCtx, RKNN_QUERY_IN_OUT_NUM, &m_rknnIoNum, sizeof(m_rknnIoNum));
    if (ret != RKNN_SUCC)
    {
        printf("rknn_query fail! ret=%d\n", ret);
        return -1;
    }
    printf("model input num: %d, output num: %d\n", m_rknnIoNum.n_input, m_rknnIoNum.n_output);

    printf("input tensors:\n");
    rknn_tensor_attr input_attrs[m_rknnIoNum.n_input];
    memset(input_attrs, 0, sizeof(input_attrs));
    for (int i = 0; i < m_rknnIoNum.n_input; i++)
    {
        input_attrs[i].index = i;
        ret = rknn_query(m_rknnCtx, RKNN_QUERY_INPUT_ATTR, &(input_attrs[i]), sizeof(rknn_tensor_attr));
        if (ret != RKNN_SUCC)
        {
            printf("rknn_query fail! ret=%d\n", ret);
            return -1;
        }
        printRKNNTensor(&(input_attrs[i]));
    }

    printf("output tensors:\n");
    rknn_tensor_attr output_attrs[m_rknnIoNum.n_output];
    memset(output_attrs, 0, sizeof(output_attrs));
    for (int i = 0; i < m_rknnIoNum.n_output; i++)
    {
        output_attrs[i].index = i;
        ret = rknn_query(m_rknnCtx, RKNN_QUERY_OUTPUT_ATTR, &(output_attrs[i]), sizeof(rknn_tensor_attr));
        if (ret != RKNN_SUCC)
        {
            printf("rknn_query fail! ret=%d\n", ret);
            return -1;
        }
        printRKNNTensor(&(output_attrs[i]));
    }

    return ret;
}

int RknnSsdModel::RknnDeInit()
{
    // Release
    if (m_rknnCtx >= 0)
    {
        rknn_destroy(m_rknnCtx);
    }
    if (m_pModel)
    {
        free(m_pModel);
    }
    return 0;
}

// fltest_opencv_rknn_ssd model/ssd_inception_v2.rknn model/road.bmp
int RknnSsdModel::DoRknnSsd(cv::Mat &src, cv::Mat &res)
{
    const int img_width = 300;
    const int img_height = 300;
    const int img_channels = 3;
    int ret = 0;

/*
    // Load image
    cv::Mat orig_img = cv::imread(img_path, 1);
    cv::Mat img = orig_img.clone();
    if (!orig_img.data)
    {
        printf("cv::imread %s fail!\n", img_path);
        return -1;
    }

    if (orig_img.cols != img_width || orig_img.rows != img_height)
    {
        printf("resize %d %d to %d %d\n", orig_img.cols, orig_img.rows, img_width, img_height);
        cv::resize(orig_img, img, cv::Size(img_width, img_height), (0, 0), (0, 0), cv::INTER_LINEAR);
    }
*/

    cv::Mat img = src.clone();
    if (src.cols != img_width || src.rows != img_height)
    {
        printf("resize %d %d to %d %d\n", src.cols, src.rows, img_width, img_height);
        cv::resize(src, img, cv::Size(img_width, img_height), (0, 0), (0, 0), cv::INTER_LINEAR);
    }

    // Set Input Data
    rknn_input inputs[1];
    memset(inputs, 0, sizeof(inputs));
    inputs[0].index = 0;
    inputs[0].type = RKNN_TENSOR_UINT8;
    inputs[0].size = img.cols * img.rows * img.channels();
    inputs[0].fmt = RKNN_TENSOR_NHWC;
    inputs[0].buf = img.data;

    ret = rknn_inputs_set(m_rknnCtx, m_rknnIoNum.n_input, inputs);
    if (ret < 0)
    {
        printf("rknn_input_set fail! ret=%d\n", ret);
        return -1;
    }

    // Run
    printf("rknn_run\n");
    ret = rknn_run(m_rknnCtx, nullptr);
    if (ret < 0)
    {
        printf("rknn_run fail! ret=%d\n", ret);
        return -1;
    }

    // Get Output
    rknn_output outputs[2];
    memset(outputs, 0, sizeof(outputs));
    outputs[0].want_float = 1;
    outputs[1].want_float = 1;
    ret = rknn_outputs_get(m_rknnCtx, m_rknnIoNum.n_output, outputs, NULL);
    if (ret < 0)
    {
        printf("rknn_outputs_get fail! ret=%d\n", ret);
        return -1;
    }

    // Post Process
    detect_result_group_t detect_result_group;
    postProcessSSD((float *)(outputs[0].buf), (float *)(outputs[1].buf), src.cols, src.rows, &detect_result_group);
    // Release rknn_outputs
    rknn_outputs_release(m_rknnCtx, 2, outputs);

    // Draw Objects
    for (int i = 0; i < detect_result_group.count; i++)
    {
        detect_result_t *det_result = &(detect_result_group.results[i]);
        printf("%s @ (%d %d %d %d) %f\n",
               det_result->name,
               det_result->box.left, det_result->box.top, det_result->box.right, det_result->box.bottom,
               det_result->prop);
        int x1 = det_result->box.left;
        int y1 = det_result->box.top;
        int x2 = det_result->box.right;
        int y2 = det_result->box.bottom;
        rectangle(src, Point(x1, y1), Point(x2, y2), Scalar(255, 0, 0, 255), 3);
        putText(src, det_result->name, Point(x1, y1 - 12), 1, 4, Scalar(0, 255, 0, 255), 4);
    }

    res = src;
    //imwrite("./out.jpg", src);

    return 0;
}
