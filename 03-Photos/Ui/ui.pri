INCLUDEPATH += $$PWD/src

HEADERS += \
    $$PWD/src/qtwidgetbase.h \
    $$PWD/src/qtpixmapbutton.h \
    $$PWD/src/qttoolbar.h \
    $$PWD/src/qtpagelistwidget.h

SOURCES += \
    $$PWD/src/qtwidgetbase.cpp \
    $$PWD/src/qtpixmapbutton.cpp \
    $$PWD/src/qttoolbar.cpp \
    $$PWD/src/qtpagelistwidget.cpp

