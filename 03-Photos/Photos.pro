QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = PhotosView
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += main.cpp

include($$PWD/photos.pri)

#temp file
DESTDIR         = $$PWD/app_bin
MOC_DIR         = $$PWD/build/photos
OBJECTS_DIR     = $$PWD/build/photos

