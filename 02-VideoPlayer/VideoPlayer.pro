QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets multimedia

TARGET = VideoPlayer
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += main.cpp

include($$PWD/videoplayer.pri)

#temp file
DESTDIR         = $$PWD/app_bin
MOC_DIR         = $$PWD/build/video
OBJECTS_DIR     = $$PWD/build/video


