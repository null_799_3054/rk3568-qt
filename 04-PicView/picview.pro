QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += main.cpp 

include($$PWD/picview.pri)

RESOURCES += \
    images.qrc

#temp file
DESTDIR         = $$PWD/build
MOC_DIR         = $$PWD/build
OBJECTS_DIR     = $$PWD/build
