﻿#include "picviewwidget.h"
#include <QLayout>
#include <QPainter>
#include <QScrollArea>

ImageBox::ImageBox(QWidget *parent)
        : QWidget(parent)
{

}

ImageBox::~ImageBox()
{
}

void ImageBox::setImage(QPixmap img)
{
    m_img = img;
    calcAndShow();
}
void ImageBox::calcAndShow()
{
    if (!m_img.isNull())
    {
        int srcWidth = m_img.width();
        int srcHeight = m_img.height();
        qDebug() << "srcWidth: " << srcWidth << ", srcHeight: " << srcHeight;

        int curWinWidth = this->width();
        int curWinHeight = this->height();
        qDebug() << "curWinWidth: " << curWinWidth << ", curWinHeight: " << curWinHeight;

        if (srcHeight / srcWidth > curWinHeight / curWinWidth)
        {
            m_newHeight = curWinHeight;
            m_newWidth = srcWidth * curWinHeight / srcHeight;
            m_scale = (float)curWinWidth / (float)srcHeight;
        }
        else
        {
            m_newHeight = srcHeight * curWinWidth / srcWidth;
            m_newWidth = curWinWidth;
            m_scale = (float)m_newWidth / (float)srcWidth;
        }
        qDebug() << "m_newWidth: " << m_newWidth << ", m_newHeight: " << m_newHeight;

        m_point = QPoint(int((curWinWidth - m_newWidth) * 0.5), int((curWinHeight - m_newHeight) * 0.5));
        qDebug() << "m_scale: " << m_scale << "m_point: " << m_point.x() << " " << m_point.y();

        update();
    }
}

void ImageBox::paintEvent(QPaintEvent *event)
{
    if (!m_img.isNull())
    {
        QPainter painter(this);
        painter.scale(m_scale, m_scale);
        if (m_wheelFlag)       // 定点缩放
        {
            m_wheelFlag = false;
            // 判断当前鼠标pos在不在图上
            float this_left_x = m_point.x() * m_lastScale;
            float this_left_y = m_point.y() * m_lastScale;
            float this_scale_width = m_newWidth * m_lastScale;
            float this_scale_height = m_newHeight * m_lastScale;

            // 鼠标点在图上，以鼠标点为中心动作
            float gap_x = m_x - this_left_x;
            float gap_y = m_y - this_left_y;
            if (0 < gap_x < this_scale_width && 0 < gap_y < this_scale_height)
            {
                int new_left_x = int(m_x / m_scale - gap_x / m_lastScale);
                int new_left_y = int(m_y / m_scale - gap_y / m_lastScale);
                m_point = QPoint(new_left_x, new_left_y);
            }
            // 鼠标点不在图上，固定左上角进行缩放
            else
            {
                int true_left_x = int(m_point.x() * m_lastScale / m_scale);
                int true_left_y = int(m_point.y() * m_lastScale / m_scale);
                m_point = QPoint(true_left_x, true_left_y);
            }
        }

        //qDebug() << "---m_scale: " << m_scale << "m_point: " << m_point.x() << " " << m_point.y();
        painter.drawPixmap(m_point, m_img);
    }
}

void ImageBox::resizeEvent(QResizeEvent *event)
{
    calcAndShow();
}

void ImageBox::wheelEvent(QWheelEvent *event)
{
    float angleY = event->delta()/8;
    m_lastScale = m_scale;
    m_wheelFlag = true;
    // 获取当前鼠标相对于view的位置
    m_x = event->x();
    m_y = event->y();
    if (angleY > 0)
    {
        m_scale *= 1.08;
    }
    else  //滚轮下滚
    {
        m_scale *= 0.92;
    }

    if (m_scale < 0.1)
    {
        m_scale = 0.1;
    }
    else if (m_scale > 100)
    {
        m_scale = 100;
    }

    adjustSize();
    update();
}

void ImageBox::mouseMoveEvent(QMouseEvent *event)
{
    if (m_leftClick)
    {
        m_endPos = event->pos() - m_startPos;          //当前位置-起始位置=差值
        m_point = m_point + m_endPos / m_scale ;       //左上角的距离变化
        m_startPos = event->pos();
        update();
    }
}

void ImageBox::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton)
    {
        m_leftClick = true;
        m_startPos = event->pos();
    }
}

void ImageBox::mouseReleaseEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton)
    {
        m_leftClick = false;
    }
}


PicViewWidget::PicViewWidget(QMainWindow *parent)
    : QMainWindow(parent)
{
    m_box = new ImageBox();
    QScrollArea *m_scrollArea = new QScrollArea();
    m_scrollArea->setMinimumSize(800, 600);
    m_scrollArea->setWidgetResizable(true);
    m_scrollArea->setWidget(m_box);

    QHBoxLayout *layout_view = new QHBoxLayout();
    layout_view->addWidget(m_scrollArea);

    m_prevBtn = new QPushButton();
    m_prevBtn->setIcon(QIcon(":/images/prev.png"));
    m_prevBtn->setFlat(true);
    m_prevBtn->setIconSize(QSize(50,50));

    m_openBtn = new QPushButton();
    m_openBtn->setIcon(QIcon(":/images/openimg.png"));
    m_openBtn->setFlat(true);
    m_openBtn->setIconSize(QSize(70,70));

    m_nextBtn = new QPushButton();
    m_nextBtn->setIcon(QIcon(":/images/next.png"));
    m_nextBtn->setFlat(true);
    m_nextBtn->setIconSize(QSize(50,50));

    m_nextBtn->setEnabled(false);
    m_prevBtn->setEnabled(false);

    QHBoxLayout *layout_button = new QHBoxLayout();
    layout_button->addWidget(m_prevBtn);
    layout_button->addWidget(m_openBtn);
    layout_button->addWidget(m_nextBtn);

    //主布局管理
    QVBoxLayout *layout_main = new QVBoxLayout();
    layout_main->setSpacing(5);
    layout_main->addLayout(layout_view);
    layout_main->addLayout(layout_button);
    layout_main->setStretch(0,10);
    layout_main->setStretch(1,1);
    layout_main->setContentsMargins(5,5,5,5);

    QWidget *widget = new QWidget();
    widget->setLayout(layout_main);
    this->setCentralWidget(widget);

    connect(m_nextBtn, SIGNAL(clicked(bool)), this, SLOT(show_next()));
    connect(m_prevBtn, SIGNAL(clicked(bool)), this, SLOT(show_prev()));
    connect(m_openBtn, SIGNAL(clicked(bool)), this, SLOT(open_files()));
}

PicViewWidget::~PicViewWidget()
{
}

void PicViewWidget::open_files()
{
    m_file = QFileDialog::getOpenFileName(this, "Open Picture", "c:", "Image Files (*.png *.jpg *.bmp)");
    qDebug() << m_file;

    if (m_file.isEmpty())
    {
        return;
    }

    QString file_path = QFileInfo(m_file).absolutePath();
    qDebug() << file_path;

    QDir dir(file_path);
    dir.setFilter(QDir::Files | QDir::NoSymLinks);
    QFileInfoList list = dir.entryInfoList(QStringList() << "*.jpg"
                                                         << "*.png"
                                                         << "*.bmp");
    m_files.clear();
    for (int i = 0; i < list.size(); i++)
    {
        QFileInfo fileInfo = list.at(i);
        qDebug() << fileInfo.absoluteFilePath();
        m_files.push_back(fileInfo.absoluteFilePath());

        if(m_file == m_files[i])
        {
            m_idx = i;
        }
    }

    qDebug() << m_files.size();
    m_box->setImage(QPixmap(m_files[m_idx]));// 显示图片
    m_prevBtn->setEnabled(true);
    m_nextBtn->setEnabled(true);
}

void PicViewWidget::show_prev()
{
    if (m_idx == 0)
    {
        m_prevBtn->setEnabled(false);
        qDebug() << m_idx;
        return;
    }
    qDebug() << m_idx;

    m_nextBtn->setEnabled(true);
    m_idx--;
    m_box->setImage(QPixmap(m_files[m_idx]));
}

void PicViewWidget::show_next()
{
    if (m_idx == m_files.length() - 1)
    {
        m_nextBtn->setEnabled(false);
        qDebug() << m_idx;
        return;
    }
    qDebug() << m_idx;

    m_prevBtn->setEnabled(true);
    m_idx++;
    m_box->setImage(QPixmap(m_files[m_idx]));
}
