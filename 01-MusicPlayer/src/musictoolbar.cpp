﻿/******************************************************************
 Copyright (C) 2019 - All Rights Reserved by
 文 件 名 : widgettoolbar.cpp --- MusicToolBar
 作 者    : Niyh(lynnhua)
 论 坛    : http://www.firebbs.cn
 编写日期 : 2019
 说 明    : 音乐播放控件(上一首、下一首、播放...)
 历史纪录 :
 <作者>    <日期>        <版本>        <内容>
  Niyh	   2019    	1.0.0 1     文件创建
 码农爱学习  2023/2/12                修改整理
*******************************************************************/
#include "musictoolbar.h"
#include "skin.h"

#include <QPainter>
#include <QMouseEvent>
#include <QDebug>
#include <QTime>

///////////////////////////////////////////////////////////////////
MusicToolBar::MusicToolBar(QWidget *parent) : QtWidgetBase(parent)
{
    m_nBaseWidth = Skin::m_nScreenWidth;
    m_nBaseHeight = 69;

    m_strCurrTime = "00:00";
    m_strDuration = "00:00";

    m_bPlaying = false;
    m_nMaxValue = 0;
    m_nCurrentValue = 0;

    InitWidget();
}

MusicToolBar::~MusicToolBar()
{
}

//进度条的进度变了
void MusicToolBar::UpdateDurationInfo(int postion)
{
    //当前位置值
    m_nCurrentValue = postion;
    QTime currentTime = GetTimeByPostion(postion);
    m_strCurrTime = currentTime.toString(m_timeFormat);
    m_progressBar->SetValue(m_nCurrentValue);

    this->update();
}

//重新设置进度条的最大值
void MusicToolBar::SetDuration(int duration)
{
    //最大位置值
    m_nMaxValue = duration;
    m_nCurrentValue = 0;
    m_progressBar->SetMaxValue(m_nMaxValue);
    m_progressBar->SetValue(m_nCurrentValue);

    //根据最大值计算要显示的总时间
    QTime totalTime = GetTimeByPostion(m_nMaxValue);
    m_timeFormat = m_nMaxValue > 3600 ? "hh:mm:ss" : "mm:ss";
    m_strDuration = totalTime.toString(m_timeFormat);
    m_strCurrTime = "00:00";

    this->update();
}

void MusicToolBar::SetPlayState(bool state)
{
    m_bPlaying = state;
    m_btns.value(1)->setChecked(state);
}

void MusicToolBar::InitWidget()
{
    // 按钮
    m_btns.insert(0, new QtPixmapButton(0, QRect(12, 4, 60, 60), QPixmap(":/images/music/ic_prev.png"), QPixmap(":/images/music/ic_prev_pre.png")));
    m_btns.insert(1, new QtPixmapButton(1, QRect(64, 4, 60, 60), QPixmap(":/images/music/ic_play.png"), QPixmap(":/images/music/ic_pause.png")));
    m_btns.insert(2, new QtPixmapButton(2, QRect(128, 4, 60, 60), QPixmap(":/images/music/ic_next.png"), QPixmap(":/images/music/ic_next_pre.png")));
    m_btns.insert(3, new QtPixmapButton(3, QRect(680, 4, 60, 60), QPixmap(":/images/music/ic_volume.png"), QPixmap(":/images/music/ic_volume_pre.png")));
    m_btns.insert(4, new QtPixmapButton(4, QRect(740, 4, 60, 60), QPixmap(":/images/music/ic_list.png"), QPixmap(":/images/music/ic_list_pre.png")));
    m_btns.value(1)->setCheckAble(true);
    connect(this, SIGNAL(signalBtnClicked(int)), this, SLOT(SltBtnClicket(int)));

    // 进度条
    m_progressBar = new QtSliderBar(this);
    m_progressBar->SetHorizontal(true);
    m_progressBar->SetMaxValue(0);
    m_progressBar->SetValue(0);
    // 进度条的值改变, 转变为MusicToolBar的信号currentPostionChanged()
    connect(m_progressBar, SIGNAL(currentValueChanged(int)), this, SIGNAL(currentPostionChanged(int)));
}

void MusicToolBar::InitProperty()
{
    m_bPlaying = false;
    m_nMaxValue = 100;
    m_nCurrentValue = 1;
}

QTime MusicToolBar::GetTimeByPostion(int postion)
{
    return QTime((postion / 3600) % 60,
                 (postion / 60) % 60,
                 postion % 60, (postion * 1000) % 1000);
}

void MusicToolBar::SltBtnClicket(int index)
{
    if (0 == index)
    {
        emit toolBarClicked(MUSIC_BTN_PREV);
    }
    else if (1 == index)
    {
        m_bPlaying = !m_bPlaying;
        m_btns.value(1)->setChecked(m_bPlaying);
        emit toolBarClicked(m_bPlaying ? MUSIC_BTN_PLAY : MUSIC_BTN_PAUSE);
    }
    else if (2 == index)
    {
        emit toolBarClicked(MUSIC_BTN_NEXT);
    }
    else if (3 == index)
    {
        emit toolBarClicked(MUSIC_BTN_VOLUMN);
    }
    else if (4 == index)
    {
        emit toolBarClicked(MUSIC_BTN_LIST);
    }
}

void MusicToolBar::resizeEvent(QResizeEvent *e)
{
    SetScaleValue();

    m_progressBar->resize(350 * m_scaleX, 30 * m_scaleX);
    m_progressBar->SetSliderSize(1, 30 * m_scaleX);
    m_progressBar->move(245 * m_scaleX, 20 * m_scaleX);
    m_progressBar->SetValue(m_progressBar->value());

    QWidget::resizeEvent(e);
}

void MusicToolBar::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    painter.setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform);
    painter.scale(m_scaleX, m_scaleY);
    painter.setBrush(QColor("#72ffffff"));
    painter.drawRect(0, 0, m_nBaseWidth, m_nBaseHeight);

    painter.setPen(QColor("#ffffff"));
    QFont font(Skin::m_strAppFontBold);
    font.setPixelSize(18);
    painter.setFont(font);

    painter.drawText(QRect(188, 15, 60, 40), Qt::AlignCenter, m_strCurrTime);
    painter.drawText(QRect(610, 15, 60, 40), Qt::AlignCenter, m_strDuration);

    foreach (QtPixmapButton *btn, m_btns)
    {
        int nX = btn->rect().left() + (btn->rect().width() - btn->pixmap().width()) / 2;
        int nY = btn->rect().top() + (btn->rect().height() - btn->pixmap().height()) / 2;
        painter.drawPixmap(nX, nY, btn->pixmap());
        // painterprintf("[%s] nX:%d, nY:%d(left:%d, top:%d, rectW:%d, rectH:%d, pixW:%d, pixH:%d)\n",
        //__func__, nX, nY, btn->rect().left(), btn->rect().top(), btn->rect().width(), btn->rect().height(), btn->pixmap().width(), btn->pixmap().height());
    }
}
